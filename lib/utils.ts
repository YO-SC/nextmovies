import { clsx, type ClassValue } from 'clsx'
import { twMerge } from 'tailwind-merge'

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs))
}

/** Intended for OMDb pagination */
export function isValidPageRange(value: number): asserts value is number {
  if (value < 1 || value > 100) {
    throw new Error('Value must be between 1 and 100.')
  }
}

/** For OMDb posters. Intended as a render flag for said posters. */
export function hasPoster(posterSrc: string) {
  if (posterSrc === 'N/A') {
    return false
  }

  return true
}

# NextMovies - OMDb Search Application

This project is a NextJS application that leverages the [OMDb API](https://www.omdbapi.com/) to enable users to search for movies, TV shows, and more. It features Server-Side Rendering (SSR) to improve performance. The frontend UI is built using the [shadcn/ui components](https://ui.shadcn.com/), offering a sleek and responsive design.

## Features

- **Search Functionality**: Quickly find movies and TV shows based on titles.
- **Fully SSR**: Enhances loading times.
- **Responsive UI**: Built with [shadcn/ui components](https://ui.shadcn.com/) for a modern and responsive user experience.

## Getting Started

To get this project up and running on your local machine, follow these steps:

### Prerequisites

- Node.js (version 20 or above)
- [bun.sh](https://bun.sh/)

### Installation

1. Clone the repository:

```bash
git clone https://gitlab.com/YO-SC/nextmovies.git
```

2. Navigate to the project directory:

```bash
cd nextmovies
```

3. Install dependencies:

```bash
bun install
```

4. Create a `.env.local` file at the root of the project and add your OMDb API key (an `.env.local.example` is in the root of the project):

```env
OMDB_API_KEY=your_api_key_here
```

5. Start the development server:

```bash
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## License

This project is open source and available under the MIT License.

## Created by

[YO-SC](https://gitlab.com/YO-SC)

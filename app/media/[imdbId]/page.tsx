import { Page } from '@/components'
import {
  TypographyH1,
  TypographyH2,
  TypographyMuted,
  TypographyP,
} from '@/components/typography'
import { OMDBTitleResult } from '@/types/omdb'
import { notFound } from 'next/navigation'
import Image from 'next/image'
import { Badge } from '@/components/ui'
import { hasPoster } from '@/lib/utils'

async function getOMDbMediaDetails(imdbId: string) {
  const response = await fetch(
    `http://www.omdbapi.com/?apiKey=${process?.env?.OMDB_API_KEY}&i=${imdbId}`,
    {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
      next: { revalidate: 3600 },
    }
  )
    .then((res) => res.json())
    .catch((error) => {
      console.error(`Error ${error}`)
    })

  return response
}

export default async function Media({
  params,
}: {
  params: { imdbId: string }
}) {
  const { imdbId } = params
  const data: OMDBTitleResult = await getOMDbMediaDetails(imdbId)

  if (data?.Error) {
    return notFound()
  }

  const {
    Title,
    Poster,
    Year,
    Type,
    Plot,
    Genre,
    Rated,
    Director,
    Writer,
    Actors,
    Released,
    Runtime,
    BoxOffice,
    Metascore,
  } = data

  return (
    <Page>
      <section className="grid grid-cols-1 md:grid-cols-2 gap-4">
        {hasPoster(Poster) && (
          <Image
            className="object-contain rounded-lg max-w-full h-full max-h-full"
            src={Poster}
            alt={`${Title}'s poster`}
            width={1920}
            height={1080}
            priority
          />
        )}

        <div>
          <div className="flex flex-col gap-2">
            <TypographyH1>{Title}</TypographyH1>
            <TypographyMuted>{Year}</TypographyMuted>
            <TypographyMuted>{Genre}</TypographyMuted>
            <Badge className="w-fit">{Type}</Badge>
          </div>

          <TypographyH2 className="mt-4">Plot</TypographyH2>
          <TypographyP>{Plot}</TypographyP>

          <TypographyH2 className="mt-4">About</TypographyH2>
          <ul className="leading-7 [&:not(:first-child)]:mt-6">
            <li>Director: {Director}</li>
            <li>Writer: {Writer}</li>
            <li>Actors: {Actors}</li>
            <li>Released: {Released}</li>
            <li>Runtime: {Runtime}</li>
            <li>Rated: {Rated}</li>
            <li>Metascore: {Metascore}</li>
            <li>Box Office: {BoxOffice}</li>
          </ul>
        </div>
      </section>
    </Page>
  )
}

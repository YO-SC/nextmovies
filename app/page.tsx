import { Page } from '@/components'
import { PaginationForm, SearchForm } from '@/components/forms'
import { TypographyH1, TypographyP } from '@/components/typography'
import {
  Badge,
  Button,
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from '@/components/ui'
import { hasPoster } from '@/lib/utils'
import { OMDBSearchRequest, OMDBSearchResult } from '@/types/omdb'
import Image from 'next/image'
import Link from 'next/link'

async function getOMDbMovies(s: string | string[], page: string | string[]) {
  const response = await fetch(
    `http://www.omdbapi.com/?apiKey=${process?.env?.OMDB_API_KEY}&s=${s}&page=${page}`,
    {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
      next: { revalidate: 3600 },
    }
  )
    .then((res) => res.json())
    .catch((error) => {
      console.error(`Error ${error}`)
    })

  return response
}

export default async function Home({
  searchParams,
}: {
  searchParams: OMDBSearchRequest
}) {
  const { s: searchTerm, page: currentPage } = searchParams
  let data

  // in order to not waste requests on '/'. OMDb issue. 1k/day limit
  if (searchTerm && currentPage) {
    data = await getOMDbMovies(searchTerm, currentPage)
  }

  return (
    <Page>
      <TypographyH1 className="md:self-center">
        Search for movies, tv shows, and more!
      </TypographyH1>

      <SearchForm />

      {data?.Error && (
        <TypographyP className="bg-red-200 p-4 rounded-lg opacity-80 md:self-center text-red-900">
          Movie not found!
        </TypographyP>
      )}

      {!data && (
        <TypographyP className="bg-secondary p-4 rounded-lg opacity-80 md:self-center">
          Start searching! Use the input above.
        </TypographyP>
      )}

      {data && !data?.Error && (
        // would be awesome for it to be a masonry layout. can be done with this https://tailwindcss.com/docs/columns but the cards overflow
        <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4">
          {data?.Search?.map((result: OMDBSearchResult) => {
            const { imdbID, Title, Year, Type, Poster } = result

            return (
              <Card className="h-fit max-w-full" key={imdbID}>
                {/* //IDEA maybe create and use a "not found" generic poster. like a user with no profile picture */}
                {hasPoster(Poster) && (
                  <Image
                    className="object-cover overflow-hidden rounded-t-lg max-w-full h-auto max-h-full"
                    src={Poster}
                    alt={`${Title}'s poster`}
                    width={1920}
                    height={1080}
                    quality={25}
                    priority
                  />
                )}
                <CardHeader>
                  <CardTitle>{Title}</CardTitle>
                  <CardDescription>{Year}</CardDescription>
                </CardHeader>
                <CardContent>
                  <Badge>{Type}</Badge>
                </CardContent>
                <CardFooter>
                  <Button className="w-full" asChild>
                    <Link href={`/media/${imdbID}`}>See details</Link>
                  </Button>
                </CardFooter>
              </Card>
            )
          })}
        </section>
      )}

      {data && !data?.Error && searchTerm && currentPage && (
        <PaginationForm searchTerm={searchTerm} currentPage={currentPage} />
      )}
    </Page>
  )
}

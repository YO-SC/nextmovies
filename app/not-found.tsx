import { Page } from '@/components'
import { TypographyH1, TypographyP } from '@/components/typography'
import { Button } from '@/components/ui'
import Link from 'next/link'

export default function NotFound() {
  return (
    <Page>
      <TypographyH1>Not Found</TypographyH1>

      <TypographyP>Could not find requested resource</TypographyP>

      <Button className="w-fit" asChild>
        <Link href={'/'}>Return home</Link>
      </Button>
    </Page>
  )
}

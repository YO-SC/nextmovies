import { TypographyP } from '@/components/typography'
import Link from 'next/link'

export function Footer() {
  return (
    <footer className="flex items-center text-sm text-muted-foreground">
      <TypographyP>
        Made by{' '}
        <Link
          className="underline underline-offset-2 hover:cursor-pointer hover:text-black"
          href={'https://gitlab.com/YO-SC'}
          target="_blank"
        >
          Ysael Sáez
        </Link>
        . Special thanks to{' '}
        <Link
          className="underline underline-offset-2 hover:cursor-pointer hover:text-black"
          href={'https://ui.shadcn.com/'}
          target="_blank"
        >
          shadcn/ui
        </Link>{' '}
        and{' '}
        <Link
          className="underline underline-offset-2 hover:cursor-pointer hover:text-black"
          href={'https://www.omdbapi.com/'}
          target="_blank"
        >
          OMDb API
        </Link>
        .
      </TypographyP>
    </footer>
  )
}

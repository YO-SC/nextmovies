export { Footer } from './Footer'
export { Navbar } from './Navbar'
export { Page } from './Page'
export { SearchMovies } from './SearchMovies'

'use client'

// ! NOT USED, this file is reserved for documentation purposes only

import {
  Badge,
  Button,
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
  Input,
  Pagination,
  PaginationContent,
  PaginationItem,
  PaginationNext,
  PaginationPrevious,
} from '@/components/ui'
import { OMDBSearchResult } from '@/types/omdb'
import Image from 'next/image'
import { useState } from 'react'
import { PiSpinnerBold } from 'react-icons/pi'
import useSWR from 'swr'
import fetch from 'unfetch'
import { TypographyP } from '@/components/typography'

const fetcher = (url: any) => fetch(url).then((r: any) => r.json())

export function SearchMovies() {
  const [title, setTitle] = useState('')
  const [shouldFetch, setShouldFetch] = useState(false)
  const [pageIndex, setPageIndex] = useState(1)
  const { data, error, isLoading } = useSWR(
    shouldFetch
      ? `http://www.omdbapi.com/?apiKey=[omdb_key]&s=${title}&page=${pageIndex}`
      : null,
    fetcher
  )

  return (
    <>
      <div className="flex w-full max-w-sm items-center space-x-2 self-center lg:max-w-[50%]">
        <Input
          name="title"
          type="text"
          placeholder="Title"
          onChange={(e) => {
            setTitle(e.target.value)
          }}
        />
        <Button
          onClick={() => {
            setShouldFetch(true)
            // setShouldFetch(false) stope fetch after data is available (to avoid re-render when typing a title)
          }}
        >
          Search
        </Button>
      </div>

      {isLoading && (
        <PiSpinnerBold className="text-4xl self-center animate-spin mt-8 text-primary" />
      )}

      {!error && !data && <TypographyP>Search for a movie!</TypographyP>}

      {error && <TypographyP>Failed to load movies.</TypographyP>}

      {data?.Error && <TypographyP>Movie not found!</TypographyP>}

      {data && !data?.Error && (
        <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4">
          {data?.Search?.map((result: OMDBSearchResult) => {
            const { imdbID, Title, Year, Type, Poster } = result

            return (
              <Card key={imdbID}>
                <Image
                  className="object-cover overflow-hidden rounded-t-lg max-w-full height-auto"
                  src={Poster}
                  alt={`${Title}'s poster`}
                  width={1920}
                  height={1080}
                  quality={50}
                />
                <CardHeader>
                  <CardTitle>{Title}</CardTitle>
                  <CardDescription>{Year}</CardDescription>
                </CardHeader>
                <CardContent>
                  <Badge>{Type}</Badge>
                </CardContent>
              </Card>
            )
          })}
        </section>
      )}

      {data && !data?.Error && (
        <Pagination>
          <PaginationContent>
            {pageIndex > 1 && (
              <PaginationItem>
                <PaginationPrevious
                  className="hover:cursor-pointer"
                  onClick={() => setPageIndex(pageIndex - 1)}
                />
              </PaginationItem>
            )}
            <PaginationItem>{pageIndex}</PaginationItem>
            <PaginationItem>
              <PaginationNext
                className="hover:cursor-pointer"
                onClick={() => setPageIndex(pageIndex + 1)}
              />
            </PaginationItem>
          </PaginationContent>
        </Pagination>
      )}
    </>
  )
}

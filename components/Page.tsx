import { cn } from '@/lib/utils'
import { HTMLAttributes } from 'react'

interface PageProps extends HTMLAttributes<HTMLDivElement> {}

export function Page(props: PageProps) {
  const { className, children, ...rest } = props

  return (
    <main className={cn('flex flex-col gap-4 ', className)} {...rest}>
      {children}
    </main>
  )
}

import {
  Pagination,
  PaginationContent,
  PaginationEllipsis,
  PaginationItem,
  PaginationLink,
  PaginationNext,
  PaginationPrevious,
} from '@/components/ui'

export function PaginationForm({
  searchTerm,
  currentPage,
}: {
  searchTerm: string | string[]
  currentPage: string | string[]
}) {
  const currentPageInt = parseInt(String(currentPage))
  const prevPage = currentPageInt - 1
  const nextPage = currentPageInt + 1
  const currentPageUrl = `/?s=${searchTerm}&page=${currentPageInt}`
  const prevPageUrl = `/?s=${searchTerm}&page=${prevPage}`
  const nextPageUrl = `/?s=${searchTerm}&page=${nextPage}`

  // TODO show actual page numbers based on actual search data. maybe using the "totalResults": "695" key

  return (
    <Pagination>
      <PaginationContent>
        {currentPageInt > 1 && (
          <>
            <PaginationItem>
              <PaginationPrevious href={prevPageUrl} />
            </PaginationItem>
            <PaginationItem>
              <PaginationLink
                href={`/?s=${searchTerm}&page=${currentPageInt - 1}`}
              >
                {currentPageInt - 1}
              </PaginationLink>
            </PaginationItem>
          </>
        )}
        <PaginationItem>
          <PaginationLink href={currentPageUrl} isActive>
            {currentPage}
          </PaginationLink>
        </PaginationItem>
        <PaginationItem>
          <PaginationLink href={`/?s=${searchTerm}&page=${currentPageInt + 1}`}>
            {currentPageInt + 1}
          </PaginationLink>
        </PaginationItem>
        <PaginationItem>
          <PaginationEllipsis />
        </PaginationItem>
        <PaginationItem>
          <PaginationNext href={nextPageUrl} />
        </PaginationItem>
      </PaginationContent>
    </Pagination>
  )
}

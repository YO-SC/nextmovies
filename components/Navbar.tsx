import { Button } from '@/components/ui'
import Link from 'next/link'
import { PiGitlabLogoFill } from 'react-icons/pi'

export function Navbar() {
  return (
    <nav className="flex justify-between items-center">
      <Link className="text-lg md:text-xl font-black" href={'/'}>
        NextMovies
      </Link>

      <span>
        <Button variant={'link'} size={'icon'} asChild>
          <Link href={'https://gitlab.com/YO-SC/nextmovies'} target="_blank">
            <PiGitlabLogoFill className="text-base" />
          </Link>
        </Button>
      </span>
    </nav>
  )
}

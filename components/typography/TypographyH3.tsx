import { cn } from '@/lib/utils'
import { TypographyHProps } from './types'

export function TypographyH3(props: TypographyHProps) {
  const { className, children, ...rest } = props

  return (
    <h3
      className={cn(
        'scroll-m-20 text-2xl font-semibold tracking-tight',
        className
      )}
      {...rest}
    >
      {children}
    </h3>
  )
}

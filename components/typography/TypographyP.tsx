import { cn } from '@/lib/utils'
import { TypographyPProps } from './types'

export function TypographyP(props: TypographyPProps) {
  const { className, children, ...rest } = props

  return (
    <p
      className={cn('leading-7 [&:not(:first-child)]:mt-6', className)}
      {...rest}
    >
      {children}
    </p>
  )
}

import { cn } from '@/lib/utils'
import { TypographyHProps } from './types'

export function TypographyH4(props: TypographyHProps) {
  const { className, children, ...rest } = props

  return (
    <h4
      className={cn(
        'scroll-m-20 text-xl font-semibold tracking-tight',
        className
      )}
      {...rest}
    >
      {children}
    </h4>
  )
}

import { cn } from '@/lib/utils'
import { TypographyHProps } from './types'

export function TypographyH1(props: TypographyHProps) {
  const { className, children, ...rest } = props

  return (
    <h1
      className={cn(
        'scroll-m-20 text-4xl font-extrabold tracking-tight lg:text-5xl',
        className
      )}
      {...rest}
    >
      {children}
    </h1>
  )
}

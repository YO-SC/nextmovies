import { cn } from '@/lib/utils'
import { TypographyHProps } from './types'

export function TypographyH2(props: TypographyHProps) {
  const { className, children, ...rest } = props

  return (
    <h2
      className={cn(
        'scroll-m-20 border-b pb-2 text-3xl font-semibold tracking-tight first:mt-0',
        className
      )}
      {...rest}
    >
      {children}
    </h2>
  )
}

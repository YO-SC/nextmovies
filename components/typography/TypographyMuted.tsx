import { cn } from '@/lib/utils'
import { TypographyPProps } from './types'

export function TypographyMuted(props: TypographyPProps) {
  const { className, children, ...rest } = props

  return (
    <p className={cn('text-sm text-muted-foreground', className)} {...rest}>
      {children}
    </p>
  )
}

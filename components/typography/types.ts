import { HTMLAttributes } from 'react'

interface TypographyHProps extends HTMLAttributes<HTMLHeadingElement> {}
interface TypographyPProps extends HTMLAttributes<HTMLParagraphElement> {}

export type { TypographyHProps, TypographyPProps }
